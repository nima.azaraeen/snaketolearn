# Lesson One

In this lesson we will create a simple HTML5 document, and do the necessary steps to create just one simple action: A snake that moves.

## Step 1: creating HTML
First a simple HTML5 page:
```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Snake Game</title>
    </head>
    <body>
    <!-- Our code goes here -->
    </body>
</html>
```
## Step 2: creating play ground
Now that we have our `HTML`page, we will use `canvas` tag to create a game board.
In the part that you see the comment `<!-- Our code goes here -->` let's put this code:
```html
<canvas id="GameBoard" width="500" height="500"></canvas>
```
but if you load the page you see nothing. that is because it is a white on white. let change that by giving our canvas a little style.  
In the `head` tag and after `title` tage we will add a `style` tag with just one style, canvas border:
```html
<style>
#GameBoard {
    border: 1px solid black;
}
</style>
```
## Step 3: creating the "Snake"
To create the snake we will start scripting with `JavaScript`.  
Just before the `body` tag ends (`</body>`) we put our `script` tag 
```html
...
    <script>
    //our code goes here
    </script>
</body>
...
``` 
and start scripting by drawing our snake. first we need to introduce our `canvas` to `javascript`:
```javascript
const GameBoard = document.getElementById("GameBoard");
```
after that we need a `context` object that is used by `JavaScript` for rendering drawings in our `canvas`
```javascript
const context = GameBoard.getContext("2d");
```
Next we create our snake by defining an `array` of `object`s that each one are coordination of one part of the snake:
```javascript
let Snake = [
    {x:250,y:250},
    {x:240,y:250},
    {x:230,y:250},
    {x:220,y:250},
    {x:210,y:250},
];
```
also to create each part of snake, or the food, we need some code that will draw a square of the size 10X10 in a place that we give it with colors we provide it.
```javascript
function drawSquare(xCoordinate, yCoordinate, squareColor, borderColor){
	context.fillStyle = squareColor;
	context.strokeStyle = borderColor;
	context.fillRect(xCoordinate, yCoordinate, 10, 10);
	context.strokeRect(xCoordinate, yCoordinate, 10, 10);
}
```
Now that we have everything we need, we can draw our snake by passing each snake part to the `drawSquare()` function, using `JavaScript` method `forEach`. For later purposes, this task will be put inside a function that we will call it `showSnake`. Also for now our snake color is `lightblue` with `black` borders.
```javascript
function showSnake(){
	Snake.forEach(element => drawSquare(element.x, element.y, 'lightblue', 'black'));
}
```
Every thing is ok, so let's give it a try by calling the function `showSnake()`. Until now your code should look something like this:
```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Snake Game</title>
        <style>
		#GameBoard {
		    border: 1px solid black;
		}
		</style>
    </head>
    <body>
    <canvas id="GameBoard" width="500" height="500"></canvas>

    <script>
    	const GameBoard = document.getElementById("GameBoard");

    	const context = GameBoard.getContext("2d");

    	let Snake = [
		    {x:250,y:250},
		    {x:240,y:250},
		    {x:230,y:250},
		    {x:220,y:250},
		    {x:210,y:250},
		];

		function drawSquare(xCoordinate, yCoordinate, squareColor, borderColor){
		    context.fillStyle = squareColor;
		    context.strokestyke = borderColor;
		    context.fillRect(xCoordinate, yCoordinate, 10, 10);
		    context.strokeRect(xCoordinate, yCoordinate, 10, 10);
		};

		function showSnake(){
			Snake.forEach(element => drawSquare(element.x, element.y, 'lightblue', 'black'));
		};

		showSnake();
    </script>
    </body>
</html>
```
## Step 4: The snake moves
Let's give life to our little snake. It is easy, just add one square to the head and remove one square from tail.  
First lets define the snake head and add 10 to head `x` coordinate so new head is one step further than old head and then add new head to the snake array using `JavaScript` method for array called `unshift()`:
```javascript
const head = {x: Snake[0].x + 10, y: Snake[0].y};
Snake.unshift(head);
```
and cut the tail using `JavaScript` method for array called `pop()`:
```javascript
Snake.pop();
```
and wrap whole thing in a function and name it `goSnake`  
finally lets call the `goSnake()` function 7 times to see snake is 7 steps further but don't forget that we must call `showSnake()` to show the moved snake:
```javascript
function goSnake(){
	const head = {x: Snake[0].x + 10, y: Snake[0].y};
	Snake.unshift(head);
	Snake.pop();
}

goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
showSnake();
```

OH NO!
there is one problem. now you see two snakes, one is the snake in first position and second is our snake after move, so we must clean our gameborad before showing snake again. that can be done by a drawing a big white square on the game board. We wrap it in a function called function called `clearBoard`
```javascript
function clearBoard() {  
	context.fillStyle = "white";  
	context.fillRect(0, 0, GameBoard.width, GameBoard.height);  
};
```
so lets call it first then tell our snake to go:
```javascript
function goSnake(){
	const head = {x: Snake[0].x + 10, y: Snake[0].y};
	Snake.unshift(head);
	Snake.pop();
}

function clearBoard() {  
    context.fillStyle = "white";  
    context.fillRect(0, 0, GameBoard.width, GameBoard.height);  
};

clearBoard();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
showSnake();
```
Hooray!!
## Step 5: Animate the moments
Now we need to make the code in a way that it shows each step automatically __wait__ some time and then show next step. there is one function in `JavaScript` that is responsible for __waiting__ called `setTimeout(a,b)` which `a` is the function we want to run are some time and `b` is the amount of waiting time in milliseconds. So if we want to draw our snake, then we tell the snake to go and draw it 1 second (1000 milliseconds) the code looks like this
```javascript
...
showSnake();

setTimeout(()=>{
clearBoard();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
goSnake();
showSnake();
}, 1000);
```
No, this doesn't look good, it looks like the snake jumps, let's go one step at a time. To do so, we will wrap `setTimeout()` in a function (let's call it `startMoving`) and then at the end of function we call it again.
so lets remove our firsts `showSnake()` because we don't need it anymore and call `goSnake()` one time. also lets run our function after declaring it.
```javascript
...
function startMoving(){
    setTimeout(()=>{
        clearBoard();
        goSnake();
        showSnake();

        startMoving();
    }, 1000);
};

startMoving();
```
## Well done!
The snake is moving and every thing is fine just lets make our timeout to 200 milliseconds because this is really slow. so your code should look like this now:
```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Snake Game</title>
        <style>
		#GameBoard {
		    border: 1px solid black;
		}
		</style>
    </head>
    <body>
    <canvas id="GameBoard" width="500" height="500"></canvas>

    <script>
    	const GameBoard = document.getElementById("GameBoard");

    	const context = GameBoard.getContext("2d");

    	let Snake = [
		    {x:250,y:250},
		    {x:240,y:250},
		    {x:230,y:250},
		    {x:220,y:250},
		    {x:210,y:250},
		];

		function drawSquare(xCoordinate, yCoordinate, squareColor, borderColor){
		    context.fillStyle = squareColor;
		    context.strokestyke = borderColor;
		    context.fillRect(xCoordinate, yCoordinate, 10, 10);
		    context.strokeRect(xCoordinate, yCoordinate, 10, 10);
		};

		function showSnake(){
			Snake.forEach(element => drawSquare(element.x, element.y, 'lightblue', 'black'));
		};

		function goSnake(){
			const head = {x: Snake[0].x + 10, y: Snake[0].y};
			Snake.unshift(head);
			Snake.pop();
		};

		function clearBoard() {  
		    context.fillStyle = "white";  
		    context.fillRect(0, 0, GameBoard.width, GameBoard.height);  
		};

		function startMoving(){
		    setTimeout(()=>{
		        clearBoard();
		        goSnake();
		        showSnake();

		        startMoving();
		    }, 200);
		};

		startMoving();
    </script>
    </body>
</html>
```
