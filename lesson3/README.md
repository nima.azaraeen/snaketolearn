# Lesson 3
In second lesson we created the snake movements and control by keyboard. in this lesson we will finish the game.
## step 1: creating food
Remember the function `drawSquare` function? Since this function has different color inputs for fill and border (stroke) we can use it with different colors to create a block of food
```javascript
drawSquare(200 , 300, 'red', 'black');
```
The problem here is that every time we use the function `clearBoard()` (each new frame), our food clears out. so we can put our food coordinations in a global variable and then call the `drawSquare()` function in the loop of each frame
```javascript
let food = {x:200,y:300}
function startMoving(){
	document.addEventListener("keydown", changeDirection);
    setTimeout(()=>{
        clearBoard();
        if(isSnakeAlive()){
	        goSnake();
	    } else {
	    	showSnake();
	    	return false;
	    }
        showSnake();
        drawSquare(food.x , food.y, 'red', 'black'); //food
        startMoving();
    }, 200);
};
```
This food only apears on the place we set for it but in the game we need to generate a food on a `random` position. Here we will create a funcion that gets three inputs, 
1. `from`: minimum number acceptible
2. `to`: maximum number acceptible
3. `step`: space between generate numbers. 
Ex. if `from` is 10, `to` is 30, step is 5, numbers generated would be 10,15,20,25,30
```javascript
function giveMeRandom(from,to,step){
	let Rand = Math.random(); //gives a number between 0(excusive) and 1 (inclusive)
	Rand *=(to-from)/step;
	Rand = Math.floor(Rand);
	Rand *= step;
	Rand +=from;
	return Rand;
};
```
so now we can have a function that generate food in random position by calling our random once for `food.x` and once for `food.y`. Also since the food should appear  in any of snake body parts we will check the coordinations that was generated and if it was the same of any of snake parts, we will call the function again.
```javascript
function giveMeFood(){
	food.x = giveMeRandom(0,GameBoard.width,10);
	food.y = giveMeRandom(0,GameBoard.height,10);
	Snake.forEach(part=>{
		if(part.x == food.x && part.y == food.y){  
			giveMefood();
		}
	});
};
```
# Step 2: Eat the food
Good, now every time Snake eats food, we need to generate a new food. Wait! what? our snake doesn't eat the food yet! Ok, so eating the food happens when coordinations of snake's head is equal to coordinations of food, in such case, we don't need to `pop()` the last element of snake array so the snake grows. So let's update our `goSnake()` function like this:
```javascript
function goSnake(){
	const head = {x: Snake[0].x + stepX, y: Snake[0].y + stepY};
	Snake.unshift(head);
	if(Snake[0].x === food.x && Snake[0].y === food.y){
		giveMeFood();
	} else {
		Snake.pop();
	}
};
```
And that's it. You have a working Snake Game. congratulations.
# Final Step
Just one little tweak and cleanup. Every time we start the game food is at `{x:200,y:300}` so we can call our `giveMeFood()` function before starting the game.
Also it is better that we move our global variables declarations to top. and finally your code should look something like this:
```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Snake Game</title>
        <style>
		#GameBoard {
		    border: 1px solid black;
		}
		</style>
    </head>
    <body>
    <canvas id="GameBoard" width="500" height="500"></canvas>

    <script>
    	let stepX=10;
		let stepY=0;
		let food = {x:200,y:300};
    	const GameBoard = document.getElementById("GameBoard");

    	const context = GameBoard.getContext("2d");

    	let Snake = [
		    {x:250,y:250},
		    {x:240,y:250},
		    {x:230,y:250},
		    {x:220,y:250},
		    {x:210,y:250},
		];

		function drawSquare(xCoordinate, yCoordinate, squareColor, borderColor){
		    context.fillStyle = squareColor;
		    context.strokestyke = borderColor;
		    context.fillRect(xCoordinate, yCoordinate, 10, 10);
		    context.strokeRect(xCoordinate, yCoordinate, 10, 10);
		};

		function showSnake(){
			Snake.forEach(element => drawSquare(element.x , element.y, 'lightblue', 'black'));
		};

		function goSnake(){
			const head = {x: Snake[0].x + stepX, y: Snake[0].y + stepY};
			Snake.unshift(head);
			if(Snake[0].x === food.x && Snake[0].y === food.y){
				giveMeFood();
			} else {
				Snake.pop();
			}
		};

		function giveMeRandom(from,to,step){
			let Rand = Math.random(); //gives a number between 0(excusive) and 1 (inclusive)
			Rand *=(to-from)/step;
			Rand = Math.floor(Rand);
			Rand *= step;
			Rand +=from;
			return Rand;
		};

		function giveMeFood(){
			food.x = giveMeRandom(0,GameBoard.width,10);
			food.y = giveMeRandom(0,GameBoard.height,10);
			Snake.forEach(part=>{
				if(part.x == food.x && part.y == food.y){  
					giveMefood();
				}
			});
		};

		function clearBoard() {  
		    context.fillStyle = "white";  
		    context.fillRect(0, 0, GameBoard.width, GameBoard.height);  
		};

		let direction = "right";
		function changeDirection(event) {
		    const pressedKey = event.keyCode;
		    const LeftKey = 37;  
		    const RightKey = 39;  
		    const UpKey = 38;  
		    const DownKey = 40;
		    switch(pressedKey) {
		        case LeftKey:
		            if(direction !== "right"){
		                stepX = -10;
		                stepY = 0;
		                direction = "left"
		            }
		            break;
		        case RightKey:
		        	if(direction !== "left"){
		        		stepX = 10;
		            	stepY = 0;
		            	direction = "right"	
		        	}
		            break;
		        case UpKey:
		        	if(direction !== "down"){
			            stepX = 0;
			            stepY = -10;
			            direction = "up"
			        }
		            break;
		        case DownKey:
		        	if(direction !== "up"){
			            stepX = 0;
			            stepY = 10;
			            direction = "down"
			        }
		            break;
		    } 
		}

		function isSnakeAlive(){
			if(
				Snake[0].x < 0 ||   // head hits the left wall
				Snake[0].x > GameBoard.width - 10 ||   // head hits the right wall
				Snake[0].y < 0 ||   // head hits the top wall
				Snake[0].y > GameBoard.height - 10   // head hits the bottom wall
			) {
				return false;
			}
			for (let i = 4; i < Snake.length; i++) {
				if (Snake[i].x === Snake[0].x && Snake[i].y === Snake[0].y){
					return false;
				}
			}
			return true;
		}

		function startMoving(){
			document.addEventListener("keydown", changeDirection);
		    setTimeout(()=>{
		        clearBoard();
		        if(isSnakeAlive()){
			        goSnake();
			    } else {
			    	showSnake();
			    	return false;
			    }
		        showSnake();
		        drawSquare(food.x , food.y, 'red', 'black'); //food
		        startMoving();
		    }, 200);
		};
		giveMeFood();
		startMoving();
    </script>
    </body>
</html>

```
# WellDone
This snake game has two logical problems and in comparison to the original game, has a minor difference. I leave these fixes to you. In the next lessons, we will add some buttons to pages to control the snake with them, we will add start and reset buttons for the game, a scorekeeper and also in order to make it more challenging, we will speed up the game as the snake grows bigger.