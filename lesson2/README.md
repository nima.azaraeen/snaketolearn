# Lesson 2
In first lesson we created the board game and a snake that moves in `x` dimension. In this lesson we will make a completely working snake game.
## step 1: move to `Y` dimension
If we take a look at our `goSnake()` function, we see that every time we are calling it, the `x` value of snake head is increased by `10`
```javascript
function goSnake(){
	const head = {x: Snake[0].x + 10, y: Snake[0].y};
	Snake.unshift(head);
	Snake.pop();
};
```
So for moving in `Y` dimension we need to do the same for `y` value of snake head. so we can change the function to have two inputs and use those inputs in our function:
```javascript
function goSnake(xChange, yChange){
	const head = {x: Snake[0].x + xChange, y: Snake[0].y + yChange};
	Snake.unshift(head);
	Snake.pop();
};
```
so now if we change the calling of `goSnake()` in the `startMoving()` like below, our snake will moving in `Y` dimension toward bottom of game board or `Down`.
```javascript
function startMoving(){
	setTimeout(()=>{
		clearBoard();
		goSnake(0,10);
		showSnake();

		startMoving();
	}, 200);
};
```
And surly, if we change `goSnake(0,10)` to `goSnake(0,-10)` it will go `UP`.
# Step 2: Getting keyboard inputs
To be able to control our snake's movements with keyboard, first we need to capture the `event` of pressing a keyboard key in our code. This can be achieved by attaching something called `event listener` to our page (document) like this:
```javascript
document.addEventListener("keydown", function(event){
    //do something
});
```
Explaining what are `eventListener`s and how `eventListener`s works is out of scope of this tutorial, but what we need to know is that the code above will add a functionality to our page like this:
evertime a key is pressed a function is called that has an `event` input. we can use this `event` input to get find which key was pressed. Each key on the keyboard has a uniq value called `keycode`. for example if we press Left arrow key, the value of `event.keycode` is `37`. here are the keys we need for this game:
```javascript
const LeftKey = 37;  
const RightKey = 39;  
const UpKey = 38;  
const DownKey = 40;
```
So, to control our snake we need to change the value of `xChange` and `yChange` in our `goSnake(xChange, yChange)` function.
Let create two global variable that keeps the value of `xChange` and `yChange` So in the `event` of key press we can chage the value of them and then feed them to our `goSnake` function. I will add them at the beginning of the script
```javascript
let stepX=10;
let stepY=0;
...
```
then when we call `goSnake()` function we can pass these variables to as an input like this:
```javascript
function startMoving(){
	setTimeout(()=>{
		clearBoard();
		goSnake(stepX,stepY);
		showSnake();

		startMoving();
	}, 200);
};
```
But since these are global variables we can use them directly inside the function and get rid of inputs
```javascript
function goSnake(){
	const head = {x: Snake[0].x + stepX, y: Snake[0].y + stepY};
	Snake.unshift(head);
	Snake.pop();
};
```
Now we neet to change the values of `stepX` and `stepY` based on keyboard input. let do that.
# Step 3: Using input to change direction
First lets create a function called `changeDirection()`, this function has one input of the `event` type that we are going to use in our `addEventListener()` function so it will look like this:
```javascript
document.addEventListener("keydown", changeDirection);
```
and the function `changeDirection()` is going to work with that event:
```javascript
function changeDirection(event) {
	const pressedKey = event.keyCode;
	const LeftKey = 37;  
	const RightKey = 39;  
	const UpKey = 38;  
	const DownKey = 40;
	switch(pressedKey) {
		case LeftKey:
			stepX = -10;
			stepY = 0;
    			break;
		case RightKey:
			stepX = 10;
			stepY = 0;
    			break;
		case UpKey:
			stepX = 0;
			stepY = -10;
    			break;
		case DownKey:
			stepX = 0;
			stepY = 10;
    			break;
	} 
}
```
and now we need to put our `addEventListener()` in a function that runs at the begining of game, So lets put it at the begining of `startMoving()`
```javascript
function startMoving(){
	document.addEventListener("keydown", changeDirection);
	setTimeout(()=>{
	...
```
Yes! that Works great. but only one problem, it should not be allowed to go left when it is going right or vice versa, same about up and down, let's fix that by adding a variable that keeps the direction of the snake and then check it when we want to change direction.
```javascript
let direction = "right";
function changeDirection(event) {
	const pressedKey = event.keyCode;
	const LeftKey = 37;  
	const RightKey = 39;  
	const UpKey = 38;  
	const DownKey = 40;
	switch(pressedKey) {
		case LeftKey:
			if(direction !== "right"){
				stepX = -10;
		                stepY = 0;
		                direction = "left"
			}
		        break;
		case RightKey:
			if(direction !== "left"){
		        	stepX = 10;
				stepY = 0;
		            	direction = "right"	
			}
		        break;
		case UpKey:
			if(direction !== "down"){
				stepX = 0;
				stepY = -10;
				direction = "up"
			}
			break;
		case DownKey:
			if(direction !== "up"){
				stepX = 0;
				stepY = 10;
				direction = "down"
			}
			break;
	} 
}
Good, that is one moving snake
```
Step 4: Let's Kill the beast
Now is the time to write the rules of dieing. Snake is dead if on of the following cases happens:
1. Snake's head hits the wall
```javascript
if(
	Snake[0].x < 0 ||   // head hits the left wall
	Snake[0].x > GameBoard.width - 10 ||   // head hits the right wall
	Snake[0].y < 0 ||   // head hits the top wall
	Snake[0].y > GameBoard.height - 10   // head hits the bottom wall
) {
	// Snake is dead
}
```
2. Snake's head goes on any part of its body (snake eats itself)  
so we will loop over all parts of the snake body and if position of the head `x` and `y` is equal to any of the body parts the snake is dead. just one thing, snake can not eat itself on the first 3 parts, so we start at 4
```javascript
for (let i = 4; i < Snake.length; i++) {
	if (Snake[i].x === Snake[0].x && Snake[i].y === Snake[0].y){
		// Snake is dead
	}
}
```
Now lets put them in a function called `isSnakeAlive()` and if it is not, we will return false.
```javascript
function isSnakeAlive(){
	if(
		Snake[0].x < 0 ||   // head hits the left wall
		Snake[0].x > GameBoard.width - 10 ||   // head hits the right wall
		Snake[0].y < 0 ||   // head hits the top wall
		Snake[0].y > GameBoard.height - 10   // head hits the bottom wall
	) {
		return false;
	}
	for (let i = 4; i < Snake.length; i++) {
		if (Snake[i].x === Snake[0].x && Snake[i].y === Snake[0].y){
			return false;
		}
	}
	return true;
}
```
Now that on each step we can check about our snake's heath!!!  we can use it in our `startMoving()` function.
```javascritp
function startMoving(){
	document.addEventListener("keydown", changeDirection);
    setTimeout(()=>{
        clearBoard();
        if(isSnakeAlive()){
	        goSnake();
	    } else {
	    	return false;
	    }
        showSnake();
        startMoving();
    }, 200);
};
```
# Well done!
Snake is now alive and by doing the wrong thing the game ends.
so your code should look like this now:
```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Snake Game</title>
        <style>
		#GameBoard {
		    border: 1px solid black;
		}
	</style>
    </head>
    <body>
    <canvas id="GameBoard" width="500" height="500"></canvas>

    <script>
    	let stepX=10;
		let stepY=0;

    	const GameBoard = document.getElementById("GameBoard");

    	const context = GameBoard.getContext("2d");

    	let Snake = [
		    {x:250,y:250},
		    {x:240,y:250},
		    {x:230,y:250},
		    {x:220,y:250},
		    {x:210,y:250},
		];

		function drawSquare(xCoordinate, yCoordinate, squareColor, borderColor){
		    context.fillStyle = squareColor;
		    context.strokestyke = borderColor;
		    context.fillRect(xCoordinate, yCoordinate, 10, 10);
		    context.strokeRect(xCoordinate, yCoordinate, 10, 10);
		};
		function showSnake(){
			Snake.forEach(element => drawSquare(element.x , element.y, 'lightblue', 'black'));
		};

		function goSnake(){
			const head = {x: Snake[0].x + stepX, y: Snake[0].y + stepY};
			Snake.unshift(head);
			Snake.pop();
		};

		function clearBoard() {  
		    context.fillStyle = "white";  
		    context.fillRect(0, 0, GameBoard.width, GameBoard.height);  
		};

		let direction = "right";
		function changeDirection(event) {
		    const pressedKey = event.keyCode;
		    const LeftKey = 37;  
		    const RightKey = 39;  
		    const UpKey = 38;  
		    const DownKey = 40;
		    switch(pressedKey) {
		        case LeftKey:
		            if(direction !== "right"){
		                stepX = -10;
		                stepY = 0;
		                direction = "left"
		            }
		            break;
		        case RightKey:
		        	if(direction !== "left"){
		        		stepX = 10;
		            	stepY = 0;
		            	direction = "right"	
		        	}
		            break;
		        case UpKey:
		        	if(direction !== "down"){
			            stepX = 0;
			            stepY = -10;
			            direction = "up"
			        }
		            break;
		        case DownKey:
		        	if(direction !== "up"){
			            stepX = 0;
			            stepY = 10;
			            direction = "down"
			        }
		            break;
		    } 
		}

		function isSnakeAlive(){
			if(
				Snake[0].x < 0 ||   // head hits the left wall
				Snake[0].x > GameBoard.width - 10 ||   // head hits the right wall
				Snake[0].y < 0 ||   // head hits the top wall
				Snake[0].y > GameBoard.height - 10   // head hits the bottom wall
			) {
				return false;
			}
			for (let i = 4; i < Snake.length; i++) {
				if (Snake[i].x === Snake[0].x && Snake[i].y === Snake[0].y){
					return false;
				}
			}
			return true;
		}

		function startMoving(){
			document.addEventListener("keydown", changeDirection);
		    setTimeout(()=>{
		        clearBoard();
		        if(isSnakeAlive()){
			        goSnake();
			    } else {
			    	showSnake();
			    	return false;
			    }
		        showSnake();
		        startMoving();
		    }, 200);
		};

		startMoving();
    </script>
    </body>
</html>
```
